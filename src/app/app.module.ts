import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SettingsComponent } from './pages/settings/settings.component';
import { ReportsComponent } from './pages/reports/reports.component';
import { TimerComponent } from './pages/timer/timer.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { fontAwesomeIcons } from './config/font-awesome-icons';
import { HeaderComponent } from './components/header/header.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { TaskComponent } from './pages/task-list/components/task/task.component';
import { GlobalListComponent } from './pages/task-list/components/global-list/global-list.component';
import { DailyTasksComponent } from './pages/task-list/components/daily-tasks/daily-tasks.component';
import { TaskListComponent } from './pages/task-list/task-list.component';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import { FilterNotTodayTasksPipe } from './pipes/filter-not-today-tasks.pipe';
import { FilterByPriorityPipe } from './pipes/filter-by-priority.pipe';
import { FilterByCategoryPipe } from './pipes/filter-by-category.pipe';
import {MatDialogModule} from '@angular/material/dialog';
import { RemoveDialogComponent } from './components/remove-dialog/remove-dialog.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatBadgeModule} from '@angular/material/badge';
import { TaskFormComponent } from './pages/task-list/components/task-form/task-form.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatRadioModule} from '@angular/material/radio';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { SignInComponent } from './pages/sign-in/sign-in.component';
import { RegistrationComponent } from './pages/registration/registration.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { IsExpiredPipe } from './pipes/is-expired.pipe';
import { IsComplitedPipe } from './pipes/is-complited.pipe';
import { AlertComponent } from './components/alert/alert.component';


@NgModule({
  declarations: [
    AppComponent,
    SettingsComponent,
    ReportsComponent,
    TimerComponent,
    HeaderComponent,
    TaskComponent,
    GlobalListComponent,
    DailyTasksComponent,
    TaskListComponent,    
    FilterNotTodayTasksPipe, FilterByPriorityPipe, FilterByCategoryPipe, RemoveDialogComponent, TaskFormComponent, SignInComponent, RegistrationComponent, ProfileComponent, IsExpiredPipe, IsComplitedPipe, AlertComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    FontAwesomeModule,
    MatToolbarModule,
    MatIconModule,
    MatTooltipModule,
    MatButtonToggleModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatBadgeModule,
    MatSlideToggleModule,
    MatRadioModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    NgCircleProgressModule.forRoot({})
    
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(iconLibrary: FaIconLibrary) {
    iconLibrary.addIcons(...fontAwesomeIcons);
  }
}
