import {
    faGear,
    faList,
    faChartSimple,
    faTrashCan,
    faPencil,
    faArrowsUpToLine,
    faEye,
    faEyeSlash,
    faPlus,
    faClock,
    faLock,
    faUnlock,    
    faCircleXmark,
    faUser,
    faArrowRightFromBracket,
    
    // Place to add icon names - faNameOfIcon
  } from '@fortawesome/free-solid-svg-icons';
  
  export const fontAwesomeIcons = [
    faGear,
    faList,
    faChartSimple,
    faTrashCan,
    faPencil,
    faArrowsUpToLine,
    faEye,
    faEyeSlash,
    faPlus,
    faClock,
    faLock,
    faUnlock,
    faCircleXmark,
    faUser,
    faArrowRightFromBracket
    // Place to add icon names - faNameOfIcon
  ];