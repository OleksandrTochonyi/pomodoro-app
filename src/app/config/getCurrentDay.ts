export function checkOnCurrentDate(value:Date) {
  let date = new Date(value)
  let taskDay = date.getDate();
  let taskMonth = date.getMonth();
  let nowDay = new Date().getDate();
  let nowMonth = new Date().getMonth();
  if (taskDay === nowDay && taskMonth === nowMonth) {
    return true;
  }
  return false;
}
