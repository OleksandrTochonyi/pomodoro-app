export function isExpiredDate(value: Date) {
  let currentDate = new Date().toLocaleDateString();
  let deadlineDate = new Date(value).toLocaleDateString();
  currentDate = currentDate.split('.').reverse().join('.')
  deadlineDate = deadlineDate.split('.').reverse().join('.')
  return new Date(currentDate) > new Date(deadlineDate)
}
