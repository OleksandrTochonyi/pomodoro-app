import { AbstractControl, FormGroup } from "@angular/forms";

export function ConfirmPasswordValidator(controlName: any, matchingControlName: any) {
    return (formGroup: FormGroup) => {
      let control = formGroup.controls[controlName];
      let matchingControl = formGroup.controls[matchingControlName]
      if (
        matchingControl.errors &&
        !matchingControl.errors.notSame
      ) {
        return;
      }
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ notSame: true });
      } else {
        matchingControl.setErrors(null);
      }
    };
  }