export interface ISettings{
    workTime: number | string,
    workIterations: number | string,
    shortBreak: number | string,
    userId?: number | string,
  }