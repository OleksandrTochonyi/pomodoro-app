export interface ITask{
    userID?: number,
    id?: number | string,
    title: string,
    description: string,
    date: any,
    priority: string,
    isComplited: boolean,
    category: string,
    onRemove: boolean
  }