export interface IUser{
    _id?: number | string,
    fullName?: string,
    email?: string,
    password?: string
  }