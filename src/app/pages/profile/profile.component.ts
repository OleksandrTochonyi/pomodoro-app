import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { RemoveDialogComponent } from 'src/app/components/remove-dialog/remove-dialog.component';
import { isExpiredDate } from 'src/app/config/isExpiredDate';
import { ITask } from 'src/app/interfaces/task.model';
import { IUser } from 'src/app/interfaces/user.model';
import { AlertService } from 'src/app/services/alert.service';
import { AuthService } from 'src/app/services/auth.service';
import { TasksService } from 'src/app/services/tasks.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  host: {
    class: 'container',
  },
})
export class ProfileComponent implements OnInit {
  user: IUser;
  tasks: ITask[] = [];
  isShowComplited: boolean = true;
  complitedTasks: number = 0;
  expiredTasks: number = 0;

  constructor(
    private authService: AuthService,
    private tasksService: TasksService,
    public dialog: MatDialog,
    public alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.authService.verifyUser().subscribe((resp) => {
      this.user = resp;
    });
    this.tasksService.getTasks().subscribe((resp) => {
      this.tasks = resp;
      this.expiredTasks = this.tasks.filter((task) =>
        isExpiredDate(task.date)
      ).length;
      this.complitedTasks = this.tasks.filter(
        (task) => task.isComplited
      ).length;
    });
  }

   // REMOVE ONE TASK

   removeTask(task: ITask): void {
    this.dialog
      .open(RemoveDialogComponent, {
        width: '500px',
        height: '400px',
        panelClass: 'remove-dialog',
        data: task,
      })
      .afterClosed()
      .subscribe((isDelete) => {
        if (isDelete) {
          this.tasksService.removeTask(task.id).subscribe((resp) => {
            this.tasks = this.tasks.filter((t) => t.id !== task.id);
            this.alertService.warning('Task was removed!')
          });
        }
      });
  }
}
