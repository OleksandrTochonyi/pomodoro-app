import { Component, OnInit } from '@angular/core';
import { Chart, registerables } from 'chart.js';
import { isExpiredDate } from 'src/app/config/isExpiredDate';
import { ITask } from 'src/app/interfaces/task.model';
import { AuthService } from 'src/app/services/auth.service';
import { TasksService } from 'src/app/services/tasks.service';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss'],
  host: {
    class: 'container',
  },
})
export class ReportsComponent implements OnInit {
  public chart: any;
  isShowPriority = true;
  tasks: ITask[] = [];
  isActual = false;
  showChart = true;

  priorityData = {
    labels: ['Urgent', 'High', 'Middle', 'Low'],
    datasets: [
      {
        label: 'Priority Graph',
        data: [0, 0, 0, 0],
        backgroundColor: ['#f75c4c', '#ffa841', '#fddc43', '#1abc9c'],
        hoverOffset: 4,
      },
    ],
  };

  categoryData = {
    labels: ['Work', 'Hobby', 'Education', 'Sport', 'Other'],
    datasets: [
      {
        label: 'Category Graph',
        data: [0, 0, 0, 0],
        backgroundColor: [
          '#ffb202',
          '#b470d0',
          '#59abe3',
          '#e16c65',
          '#00d4d9',
        ],
        hoverOffset: 4,
      },
    ],
  };

  constructor(private tasksService: TasksService) {}

  ngOnInit(): void {
    this.tasksService.getTasks().subscribe((tasks) => {
      if (!!tasks.length) {
        this.tasks = tasks;
        this.priorityData.datasets[0].data = getPriorityData(this.tasks);
        this.categoryData.datasets[0].data = getCategoriesData(this.tasks);
        Chart.register(...registerables);
        this.createChart();
      } else {
        this.showChart = false;
      }
    });
  }

  toggleTasks() {
    this.isActual = !this.isActual;
    if (this.isActual) {
      this.priorityData.datasets[0].data = getPriorityData(
        this.tasks.filter((t) => !t.isComplited && !isExpiredDate(t.date))
      );
      this.categoryData.datasets[0].data = getCategoriesData(
        this.tasks.filter((t) => !t.isComplited && !isExpiredDate(t.date))
      );
    } else {
      this.priorityData.datasets[0].data = getPriorityData(this.tasks);
      this.categoryData.datasets[0].data = getCategoriesData(this.tasks);
      this.isActual;
    }

    this.chart.destroy();
    this.createChart();
  }

  createChart(): void {
    if (this.isShowPriority) {
      this.chart = new Chart('MyChart', {
        type: 'doughnut',
        data: this.priorityData,
        options: {
          responsive: true,
          plugins: {
            legend: {
              position: 'bottom',
              labels: {
                color: '#8da5b8',
                padding: 15,
              },
            },
            title: {
              display: true,
              text: 'TASKS FILTERED BY PRIORITY',
              color: '#8da5b8',
              padding: 40,
              font: {
                size: 16,
                weight: '400',
              },
            },
          },
        },
      });
    } else {
      this.chart = new Chart('MyChart', {
        type: 'doughnut',
        data: this.categoryData,
        options: {
          responsive: true,
          plugins: {
            legend: {
              position: 'bottom',
              labels: {
                color: '#8da5b8',
                padding: 15,
              },
            },
            title: {
              display: true,
              text: 'TASKS FILTERED BY CATEGORY',
              color: '#8da5b8',
              padding: 40,
              font: {
                size: 16,
                weight: '400',
              },
            },
          },
        },
      });
    }
  }
  changeChart() {
    this.isShowPriority = !this.isShowPriority;
    this.chart.destroy();
    this.createChart();
  }
}

function getCategoriesData(tasks: ITask[]): any {
  let arr = [0, 0, 0, 0, 0];
  tasks.forEach((task) => {
    switch (task.category.toLowerCase()) {
      case 'work':
        arr[0] = arr[0] + 1;
        break;
      case 'hobby':
        arr[1] = arr[1] + 1;
        break;
      case 'education':
        arr[2] = arr[2] + 1;
        break;
      case 'sport':
        arr[3] = arr[3] + 1;
        break;
      case 'other':
        arr[4] = arr[4] + 1;
        break;
      default:
        return;
    }
  });
  return arr;
}

function getPriorityData(tasks: ITask[]): any {
  let arr = [0, 0, 0, 0];
  tasks.forEach((task) => {
    switch (task.priority.toLowerCase()) {
      case 'urgent':
        arr[0] = arr[0] + 1;
        break;
      case 'high':
        arr[1] = arr[1] + 1;
        break;
      case 'middle':
        arr[2] = arr[2] + 1;
        break;
      case 'low':
        arr[3] = arr[3] + 1;
        break;
      default:
        return;
    }
  });
  return arr;
}
