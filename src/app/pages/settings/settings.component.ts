import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';
import { AuthService } from 'src/app/services/auth.service';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
  host: {
    class: 'container',
  },
})
export class SettingsComponent implements OnInit {
  form: FormGroup;
  settings = {
    workTime: 25,
    workIterations: 5,
    shortBreak: 5,
  };

  constructor(
    private router: Router,
    private settingsService: SettingsService,
    private authService: AuthService,
    private alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.settingsService.getSettings().subscribe((resp) => {
      if (!!resp) {
        this.settings.workTime = +resp.workTime;
        this.settings.workIterations = +resp.workIterations;
        this.settings.shortBreak = +resp.shortBreak;
      } else {
        this.settingsService.setSettings(this.settings).subscribe((resp) => {
        });
      }
    });
    this.form = new FormGroup({
      workTime: new FormControl(this.settings.workTime),
      workIterations: new FormControl(this.settings.workIterations),
      shortBreak: new FormControl(this.settings.shortBreak),
    });
  }

  changeWorkTime(action: string) {
    if (action === 'inc' && this.settings.workTime < 25) {
      this.settings.workTime = this.settings.workTime + 5;
    } else if (action === 'dec' && this.settings.workTime > 5) {
      this.settings.workTime = this.settings.workTime - 5;
    }
  }

  changeWorkIterations(action: string) {
    if (action === 'inc' && this.settings.workIterations < 5) {
      this.settings.workIterations = this.settings.workIterations + 1;
    } else if (action === 'dec' && this.settings.workIterations > 2) {
      this.settings.workIterations = this.settings.workIterations - 1;
    }
  }

  changeShortBreak(action: string) {
    if (action === 'inc' && this.settings.shortBreak < 5) {
      this.settings.shortBreak = this.settings.shortBreak + 1;
    } else if (action === 'dec' && this.settings.shortBreak > 3) {
      this.settings.shortBreak = this.settings.shortBreak - 1;
    }
  }

  submit() {
    let settings = {
      workTime: this.settings.workTime,
      workIterations: this.settings.workIterations,
      shortBreak: this.settings.shortBreak,
      userId: this.authService.getCurrentUser()['_id'],
    };
    this.settingsService.updateSettings(settings).subscribe((resp) => {
      this.alertService.success('Settings was saved successfully');
    });
  }

  goToTasks(){
    this.router.navigate(['/tasks'])
  }
}
