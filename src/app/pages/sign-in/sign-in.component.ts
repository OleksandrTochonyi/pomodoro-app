import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
  host: {
    class: 'container',
  },
})
export class SignInComponent implements OnInit {
  form: FormGroup;

  constructor(public authService: AuthService) {}

  ngOnInit(): void {
    this.form = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
      ]),
    });
  }

  login() {
    if (this.form.invalid) {
      return;
    }
    this.authService.login(this.form.value).subscribe((res) => {
      this.authService
        .verifyUser()
        .subscribe((resp) => console.log('Вы успешно вошли'))
    });
  }
}
