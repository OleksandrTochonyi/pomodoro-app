import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ITask } from 'src/app/interfaces/task.model';

@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['./task-form.component.scss'],
})
export class TaskFormComponent implements OnInit {
  isNewTask: boolean = true;
  form: FormGroup;

  title: string = '';
  description: string = '';
  date: Date = new Date();
  category: any = '';
  priority: any = '';

  constructor(
    public dialogRef: MatDialogRef<TaskFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { task?: ITask; isNewTask: boolean }
  ) {}

  ngOnInit(): void {
    if (this.data.task) {
      this.title = this.data.task.title;
      this.description = this.data.task.description;
      this.date = this.data.task.date;
      this.category = this.data.task.category;
      this.priority = this.data.task.priority;
      this.isNewTask = false;
    }
    this.form = new FormGroup({
      title: new FormControl(this.title, [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(150),
      ]),
      description: new FormControl(this.description, [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(500),
      ]),
      date: new FormControl(this.date, Validators.required),
      category: new FormControl(
        this.category.toLowerCase(),
        Validators.required
      ),
      priority: new FormControl(this.priority, Validators.required),
    });
  }

  submitForm(isSave: boolean) {
    if (!isSave) {
      return false;
    }
    if (this.isNewTask) {
      return this.form.value;
    } else {
      return { ...this.data.task, ...this.form.value };
    }
  }
}
