import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { map } from 'rxjs';
import { RemoveDialogComponent } from 'src/app/components/remove-dialog/remove-dialog.component';
import { checkOnCurrentDate } from 'src/app/config/getCurrentDay';
import { ITask } from 'src/app/interfaces/task.model';


@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
 
})
export class TaskComponent implements OnInit {
  @Input() task: ITask;
  @Input() date: string;
  @Input() isRemoveMode: boolean;
  @Output() moveTaskUp = new EventEmitter<ITask>();
  @Output() remove = new EventEmitter<ITask>();
  @Output() update = new EventEmitter<ITask>();
  @Output() addToRemoveArray = new EventEmitter<ITask>();
  @Output() removeFromRemoveArray = new EventEmitter<ITask>();
  @Input() info: string;

  constructor(public dialog: MatDialog, private _router: Router) { }

  ngOnInit(): void {
  }

  moveUp(){
    this.moveTaskUp.next(this.task)
  }

  removeTask():any{
    this.remove.next(this.task)
  }

  editTask(){
    this.update.next(this.task)
  }

  isTodayDay(date:any){
    return  checkOnCurrentDate(date)
  }

  // goToTimer(task: ITask): void {
  //   this._router.navigate([`/timer`], { state: { task: task } });
  // }

  addToRemoveList(){
    this.addToRemoveArray.next(this.task)
    this.task.onRemove = !this.task.onRemove
  }

  removeFromRemoveList(){
    this.removeFromRemoveArray.next(this.task)
    this.task.onRemove = !this.task.onRemove
  }

}
