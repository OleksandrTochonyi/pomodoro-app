import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { RemoveDialogComponent } from 'src/app/components/remove-dialog/remove-dialog.component';
import { checkOnCurrentDate } from 'src/app/config/getCurrentDay';
import { isExpiredDate } from 'src/app/config/isExpiredDate';
import { ITask } from 'src/app/interfaces/task.model';
import { AlertService } from 'src/app/services/alert.service';
import { AuthService } from 'src/app/services/auth.service';
import { TasksService } from 'src/app/services/tasks.service';
import { TaskFormComponent } from './components/task-form/task-form.component';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss'],
  host: {
    class: 'container',
  },
})
export class TaskListComponent implements OnInit {
  showGlobalList: boolean = false;
  priorityValue: string = 'All';
  isRemoveMode: boolean = false;
  tasksToRemove: any[] = [];
  tasks: ITask[];

  constructor(
    public dialog: MatDialog,
    private authService: AuthService,
    private tasksService: TasksService,
    private alertService: AlertService
  ) {}

  // REMOVE ONE TASK

  removeTask(task: ITask): void {
    this.dialog
      .open(RemoveDialogComponent, {
        width: '500px',
        height: '400px',
        panelClass: 'remove-dialog',
        data: task,
      })
      .afterClosed()
      .subscribe((isDelete) => {
        if (isDelete) {
          this.tasksService.removeTask(task.id).subscribe((resp) => {
            this.tasks = this.tasks.filter((t) => t.id !== task.id);
            this.alertService.warning('Task was removed!');
          });
        }
      });
  }

  // ADD TASK

  addTask(): void {
    this.dialog
      .open(TaskFormComponent, {
        width: '800px',
        height: '800px',
        panelClass: 'task-form',
        data: { isNew: true },
      })
      .afterClosed()
      .subscribe((data) => {
        if (data) {
          let userId = this.authService.getCurrentUser()['_id'];
          let newTask = {
            ...{
              id: getRandomId(),
              onRemove: false,
              isComplited: false,
              userId,
            },
            ...data,
          };
          this.tasksService.addTask(newTask).subscribe(
            (task) => {
              this.tasks = [...this.tasks, task];
              this.alertService.success('Task was successfully added!');
            },
            (err) => {
              this.alertService.error(err.message);
            }
          );
        }
      });
  }

  //SET PRIORITY VALUE FN

  setPriorityValue(value: string) {
    this.priorityValue = value;
  }

  //MOVE TASK UP

  moveTaskUp(task: ITask) {
    const todayTasks = this.tasks.filter((task) =>
      checkOnCurrentDate(task.date)
    );
    if (todayTasks.length > 4) {
      return;
    }
    this.tasksService
      .updateTask({ ...task, date: new Date() })
      .subscribe((resp) => {
        this.tasks = this.tasks.map((task) => {
          if (task.id === resp.id) {
            return resp;
          } else {
            return task;
          }
        });
        this.alertService.info('Task was moved up!');
      });
  }

  //UPDATE TASK

  updateTask(task: ITask): any {
    this.dialog
      .open(TaskFormComponent, {
        width: '800px',
        height: '800px',
        panelClass: 'task-form',
        data: { task, isNew: false },
      })
      .afterClosed()
      .subscribe((task) => {
        if (task) {
          this.tasksService.updateTask(task).subscribe((resp) => {
            this.tasks = this.tasks.map((task) => {
              if (task.id === resp.id) {
                return resp;
              } else {
                return task;
              }
            });
            this.alertService.success('Task was edited!');
          });
        }
      });
  }

  //TOGGLE IN REMOVE MODE

  toggleInRemoveArr(task: ITask, type: string) {
    if (type === 'add') {
      this.tasksToRemove.push(task.id);
    } else {
      this.tasksToRemove = this.tasksToRemove.filter((id) => id !== task.id);
    }
  }

  ngOnInit(): void {
    this.tasksService.getTasks().subscribe((tasks) => {
      this.tasks = tasks;
    });
  }

  // CATEGORY FILTER

  isCategory(category: string, priority: string) {
    let tasksToFilter = this.tasks.filter(
      (task) => !checkOnCurrentDate(task.date) && !isExpiredDate(task.date)
    );

    let tasks = tasksToFilter.filter(
      (task) => task.category.toLowerCase() === category.toLowerCase()
    );
    if (priority.toLowerCase() === 'all') {
      return !!tasks.length;
    }
    tasks = tasks.filter(
      (task) => task.priority.toLowerCase() === priority.toLowerCase()
    );
    return !!tasks.length;
  }

  //REMOVE MODE

  useTrashInRemoveMode() {
    if (!this.tasks) {
      return;
    }
    if (this.tasksToRemove.length > 0) {
      this.dialog
        .open(RemoveDialogComponent, {
          width: '500px',
          height: '400px',
          panelClass: 'remove-dialog',
          data: this.tasksToRemove.length,
        })
        .afterClosed()
        .subscribe((isDelete) => {
          if (isDelete) {
            this.tasksService
              .removeTasksArray(this.tasksToRemove)
              .subscribe((res) => {
                this.tasks = this.tasks.filter(
                  (task) => !this.tasksToRemove.includes(task.id)
                );
                this.alertService.warning(
                  `${this.tasksToRemove.length} task(s) was removed!`
                );
                this.isRemoveMode = false;
                this.tasksToRemove = [];
              });
          } else {
            this.isRemoveMode = false;
            this.tasksToRemove = [];
            this.toggleOnRemoveInTheTask('turn off');
          }
        });
    } else {
      this.isRemoveMode = !this.isRemoveMode;
    }
  }

  //TOGGLE FN IN REMOVE MODE

  toggleOnRemoveInTheTask(type: string) {
    if (type === 'turn on') {
      this.tasks = this.tasks.map((task) => {
        task.onRemove = true;
        return task;
      });
    } else {
      this.tasks = this.tasks.map((task) => {
        task.onRemove = false;
        return task;
      });
    }
  }

  //SELECT or DESELECT ALL TASKS IN REMOVE MODE

  toggleRemoveTasks(type: string) {
    if (type === 'select all') {
      this.toggleOnRemoveInTheTask('turn on');
      this.tasksToRemove = this.tasks.map((task) => {
        return task.id;
      });
    } else {
      this.toggleOnRemoveInTheTask('turn off');
      this.tasksToRemove = [];
    }
  }

  // CHECK ON ALL TASKS TO REMOVE

  checkOnAllTasks() {
    return this.tasksToRemove.length === this.tasks.length;
  }

  isTasks(when: string): boolean {
    if (!this.tasks) {
      return true;
    }
    let actualTasks = this.tasks.filter(task=>!task.isComplited)
    let tasks = actualTasks.filter(task=>!isExpiredDate(task.date))
    if (when === 'today') {
      return tasks.some((task) => checkOnCurrentDate(task.date));
    } else {
      return tasks.some((task) => !checkOnCurrentDate(task.date));
    }
  }
}

function getRandomId() {
  return '_' + Math.random().toString(36).substr(2, 9);
}
