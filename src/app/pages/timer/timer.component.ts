import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import {
  finalize,
  map,
  Observable,
  share,
  Subscription,
  take,
  timer,
} from 'rxjs';
import { ITask } from 'src/app/interfaces/task.model';
import { SettingsService } from 'src/app/services/settings.service';
import { TasksService } from 'src/app/services/tasks.service';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss'],
  host: {
    class: 'container',
  },
})
export class TimerComponent implements OnInit {
  task: ITask | any;
  taskSettings: any;
  time: number;
  subtitle = 'Work Iteration';
  shortBreak: number;
  isComplited = false;
  fillTomatoArr: string[] = [];
  emptyTomatosArr: string[] = [];
  fullIterationsCount: number;
  secondsInMinute = 1;

  timer$: Observable<number>;
  timerSub: Subscription;
  disableStart = false;
  disableReset = true;
  disablePause = true;
  isWorkIterationComplited = false;
  isShowNext = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private tasksService: TasksService,
    private settingsService: SettingsService
  ) {
    this.settingsService.getSettings().subscribe((resp) => {
      this.taskSettings = resp;
      this.time = +this.taskSettings.workTime * this.secondsInMinute;
      this.shortBreak = +this.taskSettings.shortBreak * this.secondsInMinute;
      this.fullIterationsCount = +this.taskSettings.workIterations;
      this.emptyTomatosArr = Array(this.fullIterationsCount).fill('empty');
    });
  }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.tasksService.getOneTaskById(params.id).subscribe((resp) => {
        this.task = resp;
      });
    });
  }

  startTimer() {
    if (this.fullIterationsCount === this.fillTomatoArr.length) {
      console.log('Ты уже и так хорошо поработал, вали погуляй)');
      return;
    }
    this.timerSub && this.timerSub.unsubscribe();
    this.disableStart = true;
    this.disableReset = false;
    this.disablePause = false;
    this.timer$ = timer(0, 1000).pipe(
      map((i) => {
        this.time = this.time - 1;
        return this.time;
      }),
      take(this.time),
      finalize(() => {
        if (this.time === 0) {
          setTimeout(() => {
            if (this.isWorkIterationComplited) {
              this.toggleTomato();
            }
            this.disablePause = true;
            this.disableReset = true;
            this.isWorkIterationComplited = !this.isWorkIterationComplited;
            this.isShowNext = true;
            this.subtitle = 'Complited';
            this.playAudio();
          }, 1000);
        }
      }),
      share()
    );
    this.timerSub = this.timer$.subscribe();
  }

  pauseTimer() {
    this.timerSub.unsubscribe();
    this.disableStart = false;
    this.disablePause = true;
  }

  goNextIteration() {
    if (this.isWorkIterationComplited) {
      this.time = this.taskSettings.shortBreak;
      this.subtitle = 'Break';
    } else {
      this.time = this.taskSettings.workTime;
      this.subtitle = 'Work Iteration';
    }
    this.disableStart = false;
    this.disableReset = true;
    this.disablePause = true;
    this.isShowNext = false;
  }

  resetTimer() {
    this.disableStart = false;
    this.disableReset = true;
    this.disablePause = true;
    this.timerSub.unsubscribe();
    this.time = this.taskSettings.workTime * this.secondsInMinute;
    this.isWorkIterationComplited = false;
  }

  getTime(timeInSec: number) {
    let seconds = timeInSec;
    let minutes = 0;
    let hours = 0;
    if (seconds >= 60) {
      minutes = Math.floor(seconds / 60);
      seconds = Math.floor(seconds % 60);
      if (minutes >= 60) {
        hours = Math.floor(minutes / 60);
        minutes = Math.floor(minutes % 60);
      }
    }
    return { hours, minutes, seconds };
  }

  getTimeLineInPersents(value: string) {
    if (!!this.taskSettings) {
      if (value === 'Work') {
        return 100 - (this.time / this.taskSettings.workTime) * 100;
      } else {
        return 100 - (this.time / this.taskSettings.shortBreak) * 100;
      }
    } else return 0;
  }

  playAudio() {
    let audio = new Audio();
    audio.src = '../../../assets/sounds/complete-sound.m4a';
    audio.load();
    audio.play();
  }

  finishTask() {
    this.tasksService
      .updateTask({ ...this.task, isComplited: true })
      .subscribe((resp) => {});
    this.router.navigate(['/tasks']);
  }

  toggleTomato() {
    this.fillTomatoArr.push('tomato');
    this.emptyTomatosArr.pop();
  }
}
