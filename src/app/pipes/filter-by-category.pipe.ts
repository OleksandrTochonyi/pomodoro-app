import { Pipe, PipeTransform } from '@angular/core';
import { ITask } from '../interfaces/task.model';

@Pipe({
  name: 'filterByCategory'
})
export class FilterByCategoryPipe implements PipeTransform {

  transform(tasks: ITask[], category: string): ITask[] {
    if(category===''){
      return tasks;
    }
    return tasks.filter(task=>task.category.toLowerCase() === category.toLowerCase());
  }

}
