import { Pipe, PipeTransform } from '@angular/core';
import { ITask } from '../interfaces/task.model';

@Pipe({
  name: 'filterByPriority',
})
export class FilterByPriorityPipe implements PipeTransform {
  transform(tasks: ITask[], priority: string): ITask[] {
    if (priority.toLocaleLowerCase() === 'all') {
      return tasks;
    }
    return tasks.filter(
      (task) => task.priority.toLowerCase() === priority.toLocaleLowerCase()
    );
  }
}
