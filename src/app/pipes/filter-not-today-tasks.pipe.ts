import { Pipe, PipeTransform } from '@angular/core';
import { checkOnCurrentDate } from '../config/getCurrentDay';
import { ITask } from '../interfaces/task.model';

@Pipe({
  name: 'filterNotTodayTasks',
})
export class FilterNotTodayTasksPipe implements PipeTransform {
  transform(tasks: ITask[], date: string): ITask[] {
    if(!tasks){
      return []
    }
    return tasks.filter((task) => {
      if (date === 'today') {
        if (checkOnCurrentDate(task.date)) {
          return true;
        }
        return false;
      } else {
        if (!checkOnCurrentDate(task.date)) {
          return true;
        }
        return false;
      }
    });
  }
}
