import { Pipe, PipeTransform } from '@angular/core';
import { ITask } from '../interfaces/task.model';

@Pipe({
  name: 'isComplited',
})
export class IsComplitedPipe implements PipeTransform {
  transform(tasks: ITask[], value: string): ITask[] {
    if (!tasks) {
      return [];
    }
    if(value==='complited'){
      return tasks.filter((task) => task.isComplited);
    }else{
      return tasks.filter((task) => !task.isComplited);
    }
    
  }
}
