import { Pipe, PipeTransform } from '@angular/core';
import { isExpiredDate } from '../config/isExpiredDate';
import { ITask } from '../interfaces/task.model';

@Pipe({
  name: 'isExpired',
})
export class IsExpiredPipe implements PipeTransform {
  transform(tasks: ITask[], expired: string): ITask[] {
    if (!tasks) {
      return [];
    }
    if (expired === 'expired') {
      return tasks.filter((task) => isExpiredDate(task.date));
    } else {
      return tasks.filter((task) => !isExpiredDate(task.date));
    }
  }
}
