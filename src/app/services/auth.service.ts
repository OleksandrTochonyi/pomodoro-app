import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, Observable, tap, throwError } from 'rxjs';
import { SERVER_URL } from '../helpers/constants';
import { IUser } from '../interfaces/user.model';
import { StorageService } from './storage.service';
import { AlertService } from 'src/app/services/alert.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  isAuth = false;
  token: any;
  currentUser: IUser;

  constructor(
    private router: Router,
    private storageService: StorageService,
    private http: HttpClient,
    private alertService: AlertService
  ) {
    if (this.storageService.getFromSessionStorage()) {
      this.token = this.storageService.getFromSessionStorage();
      this.verifyUser().subscribe((resp) => resp);
    }
  }

  login(user: any): Observable<any> {
    return this.http.post(SERVER_URL + '/login', user).pipe(
      tap((resp) => {
        this.isAuth = true;
        this.token = resp;
        this.storageService.setInSessionStorage(resp);
        this.router.navigate(['/tasks']);
      }),
      catchError(this.handlerError.bind(this))
    );
  }

  handlerError(error: HttpErrorResponse) {
    this.alertService.error(error.error);
    return throwError(error);
  }

  logout() {
    this.isAuth = false;
    this.storageService.removeFromSessionStorage();
    this.router.navigate(['/login']);
  }

  isAutentificated() {
    let isAuth = this.storageService.getFromSessionStorage();
    if (isAuth) {
      this.isAuth = isAuth;
    }
    return this.isAuth;
  }

  registration(user: any): Observable<any> {
    return this.http.post(SERVER_URL + '/user', user);
  }

  verifyUser(): Observable<any> {
    return this.http.post(SERVER_URL + '/token', this.token).pipe(
      tap((user) => {
        this.currentUser = user;
      })
    );
  }

  getCurrentUser(): IUser {
    return this.currentUser;
  }

  getToken() {
    return this.token;
  }
}
