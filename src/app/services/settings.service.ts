import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, pipe, tap } from 'rxjs';
import { SERVER_URL } from '../helpers/constants';
import { ISettings } from '../interfaces/settings.model';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root',
})
export class SettingsService {
  defaultSettings = {
    workTime: 25,
    workIterations: 5,
    shortBreak: 5,
  };

  constructor(private http: HttpClient, private authService: AuthService) {}

  getSettings(): Observable<any> {
    let token = this.authService.getToken().token;
    let headers = new HttpHeaders().set('authorization', token);
    return this.http.get(SERVER_URL + '/settings', { headers: headers }).pipe(
      tap((resp) => {
        if (!resp) {
          return this.defaultSettings;
        } else {
          return resp;
        }
      })
    );
  }

  /**
   *
   * @param settings
   * @returns
   */
  setSettings(settings: ISettings): Observable<any> {
    const userId = this.authService.getCurrentUser()._id;
    return this.http.post(SERVER_URL + '/settings', { ...settings, userId });
  }

  /**
   * Function to do PUT call to backend to update ISettings
   *
   * @param settings - ISettings updated entity
   * @returns Observable of type any
   */
  updateSettings(settings: ISettings): Observable<any> {
    return this.http.put(SERVER_URL + `/settings/${settings.userId}`, settings);
  }
}
