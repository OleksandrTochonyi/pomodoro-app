import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  key = 'user';

  constructor() { }

  setInSessionStorage(value: any){
    sessionStorage.setItem(this.key, JSON.stringify(value))
  }

  getFromSessionStorage(): any{
    let dataFromStorage = sessionStorage.getItem(this.key);
    if(!dataFromStorage){   
      return null;
    }
    return JSON.parse(dataFromStorage)
  }

  removeFromSessionStorage(){
    sessionStorage.removeItem(this.key)
  }
}
