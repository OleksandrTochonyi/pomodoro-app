import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SERVER_URL } from '../helpers/constants';
import { ITask } from '../interfaces/task.model';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root',
})
export class TasksService {
  constructor(private http: HttpClient, private authService: AuthService) {}
  getTasks(): Observable<any> {
    let token = this.authService.getToken().token;
    let headers = new HttpHeaders().set('authorization', token);
    return this.http.get(SERVER_URL + '/tasks', { headers: headers });
  }

  addTask(task: ITask): Observable<any> {
    return this.http.post(SERVER_URL + '/tasks', task);
  }

  removeTask(taskId: any): Observable<any> {
    return this.http.delete(SERVER_URL + `/tasks/${taskId}`);
  }

  updateTask(task: ITask): Observable<any> {
    return this.http.put(SERVER_URL + `/tasks/${task.id}`, task);
  }

  removeTasksArray(tasks: ITask[]): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: tasks,
    };
    return this.http.delete(SERVER_URL + '/tasks', options);
  }

  getOneTaskById(taskId: string): Observable<any>{
    return this.http.get(SERVER_URL + `/tasks/${taskId}`)
  }
}


